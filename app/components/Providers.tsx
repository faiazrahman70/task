'use client'
import { SessionProvider } from 'next-auth/react'
import React, { ReactNode } from 'react'

interface providerProps {
	children: ReactNode
}

export default function Providers(props: providerProps) {
	return <SessionProvider>{props.children}</SessionProvider>
}
