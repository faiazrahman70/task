import React from 'react'

import { faGithub, faGoogle } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { useSession, signIn, signOut } from 'next-auth/react'

interface loginProps {
	loginType: string
}

export default function LoginButton(props: loginProps) {
	if (props.loginType === 'github') {
		return (
			<>
				<button
					onClick={() => {}}
					className="flex text-sm border-solid 
                    border-slate-200
              pl-16 pr-16 pt-5 pb-5 gap-4
              border-[1px] justify-center items-center 
              shadow_style
              transition-shadow duration-700
            "
				>
					<FontAwesomeIcon icon={faGithub} size="2x" />
					<p className="text-slate-300">Login With Github</p>
				</button>
			</>
		)
	} else {
		return (
			<button
				onClick={() => {
					signIn()
				}}
				className="flex text-sm border-solid 
					border-slate-200
          pl-16 pr-16 pt-5 pb-5 gap-4 mb-6
          border-[1px] justify-center 
		  items-center shadow_style
          transition-shadow duration-700
        "
			>
				<FontAwesomeIcon icon={faGoogle} size="2x" />
				<p className="text-slate-300">Login With Google</p>
			</button>
		)
	}
}
