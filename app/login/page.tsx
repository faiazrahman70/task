'use client'

import React from 'react'

import '../styles/custom-styles.css'

import LoginButton from '../components/LoginButton'

export default function Login() {
	return (
		<div className="flex min-h-screen flex-col items-center pt-10">
			<div
				className="flex w-screen justify-center
        items-center
      "
			>
				<p className="text-5xl text-white ">Login</p>
			</div>

			<div
				className="flex flex-1 flex-grow w-screen
        flex-col justify-center items-center
      "
			>
				{/* ====>  github _ button */}
				<LoginButton loginType="google" />

				<LoginButton loginType="github" />

				{/* ====> google _ button */}
			</div>
		</div>
	)
}
