'use client'
import React, { useEffect, useState } from 'react'

import { useSession } from 'next-auth/react'

export default function Ekyc() {
	const { data: session } = useSession()

	const [url, setUrl] = useState<any>()
	const [name, setName] = useState<any>()

	useEffect(() => {
		if (session) {
			setUrl(session.user?.image)
			setName(session.user?.name)
		}
	}, [])

	return (
		<div
			className="flex 
                flex-col w-screen 
                bg-red-100 h-screen
                "
		>
			<img src={url} alt="" className="" />
			<p
				className="font-sans text-black
                text-sm
            "
			>
				{name}
			</p>
		</div>
	)
}
