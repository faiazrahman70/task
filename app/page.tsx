'use client'
import { useState, useEffect } from 'react'
import { SessionProvider } from 'next-auth/react'

import './styles/custom-styles.css'

import Login from './login/page'
import Ekyc from './eKYC/page'

import { useSession } from 'next-auth/react'

export default function Home() {
	const { data: session } = useSession()

	// console.log('session', session)
	useEffect(() => {
		if (session) {
			console.log(session.user)
		}
	}, [session])

	return <>{session ? <Ekyc /> : <Login />}</>
}
